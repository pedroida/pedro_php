<?php
require_once('config.php');

class Store
{
    private $product;
    private $validate;
    private $find;

    public function __construct()
    {
        $this->validate = new ValidateStore($_POST, $_FILES['image']['name']);
        $this->checkReturn();
    }

    protected function checkReturn()
    {
        if ($this->validate->validateInputs()) {
            $this->find = new Find();
            $this->insert();
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    protected function fixPrice($price)
    {
        $source = array('.', ',');
        $replace = array('', '.');
        $price = str_replace($source, $replace, $price);

        return $this->product->setPrice($price);
    }

    protected function productCreateSql()
    {
        $title = $this->product->getTitle();
        $description = $this->product->getDescription();
        $price = $this->product->getPrice();
        $image = $this->product->getImage();

        return $sql = "INSERT INTO products (title, description, price, image)
                VALUES ('$title', '$description', '$price', '$image')";

    }

    protected function createDirectory()
    {
        $this->product = $this->find->getProductStore();

        $old = umask(0);
        $dir = 'products/' . $this->product->getId() . "/";
        mkdir($dir, 0777, true) or die("erro ao criar diretório");
        umask($old);

        return $dir;
    }

    protected function uploadImage()
    {
        $dir = $this->createDirectory();
        move_uploaded_file($_FILES['image']['tmp_name'], $dir . $this->product->getImage());
    }

    public function insert()
    {

        $this->product = new Product(
            $_POST['title'],
            $_POST['description'],
            $_POST['price'],
            $_FILES['image']['name']
        );
        $this->fixPrice($this->product->getPrice());

        try {
            if (!mysqli_query(Connection::connect(), $this->productCreateSql())) {
                throw new mysqli_sql_exception("Erro de inserção", 400);
            }

            $this->uploadImage();

            header('location: index.php');
        } catch (mysqli_sql_exception $exception) {
            echo json_encode($exception);
        } finally {
            Connection::connect()->close();
        }
    }
}

$store = new Store();
