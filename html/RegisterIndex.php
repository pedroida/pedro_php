<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 28/02/18
 * Time: 11:15
 */
session_start();
session_start();
require_once('config.php');


class RegisterIndex
{
    private $user;
    private $inputs;
    private $validate;

    public function __construct()
    {
        $this->inputs = $_POST;
        $this->validate = new ValidateRegister($this->inputs);
    }

    public function checkInputs()
    {
        if ($this->validate->validateInputs()) {
            $this->registerUser();
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    protected function populateUser()
    {
        $this->user = new User($this->inputs['email'], $this->inputs['password']);
        $this->user->setName($this->inputs['name']);
        $this->user->setPicture("images/default.png");
    }

    protected function giveSql()
    {
        $this->populateUser();

        $name = $this->user->getName();
        $email = $this->user->getEmail();
        $password = $this->user->getPassword();
        $picture = $this->user->getPicture();

        $query = "INSERT INTO users (name, email, password, pic)
                  VALUES ('$name', '$email', '$password', '$picture')";

        return $query;

    }

    protected function registerUser()
    {
        try {
            Connection::connect()->query($this->giveSql());
            $name = $this->user->getName();
            $picture = $this->user->getPicture();

            $_SESSION['name'] = $name;
            $_SESSION['picture'] = $picture;
            $_SESSION['logged'] = true;
            header('Location: index.php');
        } catch (mysqli_sql_exception $ex) {
            echo json_encode($ex);
        }
    }

}

$register = new RegisterIndex();
$register->checkInputs();
