$(document).ready(function() {
    $(".price").maskMoney({
        showSymbol: true,
        symbol: "R$",
        decimal: ",",
        thousands: "."
    }).val();

    $('.price').each(function() { // function to apply mask on load!
        $(this).maskMoney('mask', $(this).val());
    });
});
