<?php
require_once('config.php');

class Delete
{
    public function __construct()
    {
    }

    protected function getId()
    {
        return $_POST['id'];
    }

    protected function giveSql()
    {
        return "DELETE FROM products WHERE id=" . $this->getId();
    }

    public function deleteDirectory($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public function delete()
    {
        $dir = 'products/' . $this->getId() . "/";

        $this->deleteDirectory($dir);

        if (mysqli_query(Connection::connect(), $this->giveSql())) {

            header('location: index.php');
        }
        Connection::connect()->close();
    }
}

VerifyLogin::isLogged();

$delete = new Delete();
$delete->delete();
