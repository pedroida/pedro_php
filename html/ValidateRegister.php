<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 28/02/18
 * Time: 11:51
 */

class ValidateRegister
{
    private $inputs;

    public function __construct(array $inputs)
    {
        $this->inputs = $inputs;
    }

    public function validateInputs()
    {
        foreach ($this->inputs as $input) {
            if (empty(trim($input))) {
                $this->invalidResponse();
                return false;
            }
        }

        if (!$this->isValidPassword()) {
            return false;
        }

        if (!$this->isValidEmail($this->inputs['email'])) {
            return false;
        }

        return true;
    }

    protected function invalidResponse()
    {
        return $_SESSION['error_register'] = 'Dados inválidos';
    }

    protected function isValidPassword()
    {
        if ($this->inputs['password'] != $this->inputs['password_confirmation']) {
            $this->invalidResponse();
            return false;
        }
        return true;
    }

    protected function isValidEmail($email)
    {
        $result = Connection::connect()->query("SELECT * FROM users WHERE email='$email'");

        if (mysqli_num_rows($result) > 0 || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->invalidResponse();
            return false;
        }

        return true;
    }
}