<?php

class Connection
{
    public static function connect()
    {
        $conn = new mysqli("localhost", "root", "batata02", "teste_lets");

        if ($conn->connect_error) {
            echo $conn->connect_error;
        }

        return $conn;
    }
}
