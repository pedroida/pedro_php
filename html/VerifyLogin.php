<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 28/02/18
 * Time: 10:08
 */

class VerifyLogin
{
    public static function isLogged()
    {
        if (!$_SESSION['logged']) {
            header('location:login.php');
            return false;
        } else {
            return true;
        }
    }
}