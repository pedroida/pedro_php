<?php
require_once('config.php');

session_start();

require_once('header.php');
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Entrar</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="LoginIndex.php">
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Email: </label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Senha: </label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>
                        <?php
                        if ($_SESSION['error_login']) {
                            ?>
                            <div class="form-group col-md-6">
                                Dados Incorretos!
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Entrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include_once('footer.php'); ?>
</html>
