<?php
require_once('config.php');

session_start();

VerifyLogin::isLogged();

$product = $_SESSION['product'];

require_once('header.php');
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editar Produto - <?php echo $product->getTitle(); ?>
                </div>
                <div class="panel-body well">
                    <form id="myForm" action="Update.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $product->getId(); ?>">
                        <div class="form-group text-center">
                            <img style="max-width:200px;" name="image" src="<?php echo $product->getPathImage(); ?>"
                                 alt="<?php echo $product->getTitle(); ?>">
                        </div>
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" value="<?php echo $product->getTitle(); ?>"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="description">Descrição</label>
                            <input type="text" name="description" value="<?php echo $product->getDescription(); ?>"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="price">Preço</label>
                            <input type="text" name="price" value="<?php echo $product->getPrice(); ?>"
                                   class="form-control price">
                        </div>
                        <div class="form-group">
                            <label for="image">Imagem</label>
                            <input type="file" name="image" class="form-control">
                        </div>

                        <button class="btn btn-success" type="submit" name="button">Salvar</button>
                        <a class="btn btn-info" href="index.php">Voltar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include_once('footer.php'); ?>
</html>
<script type="text/javascript" src="js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="js/masks.js"></script>
