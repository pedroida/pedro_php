<?php
require_once('config.php');

class Update
{
    private $id;
    private $title;
    private $description;
    private $price;
    private $image;
    private $sql;
    private $setColumns;

    public function __construct()
    {

        $this->id = $_POST['id'];
        $this->title = $_POST['title'];
        $this->description = $_POST['description'];
        $this->price = $_POST['price'];
        $this->image = $_FILES['image']['name'];
        $this->sql = "UPDATE products SET ";
        $this->setColumns = [];
    }

    protected function populateSql()
    {
        if (!empty(trim($this->title))) {
            array_push($this->setColumns, "title=" . "'$this->title'");
        }

        if (!empty(trim($this->description))) {
            array_push($this->setColumns, 'description=' . "'$this->description'");
        }

        if (!empty(trim($this->price)) && $this->price != '0,00') {
            $this->fixPrice($this->price);

            array_push($this->setColumns, 'price=' . "'$this->price'");
        }
        if (!empty(trim($this->image))) {
            $this->isImage();
            array_push($this->setColumns, 'image=' . "'$this->image'");
        }
        $this->checkSize();
        $this->implodeSqlColumns();
    }

    protected function fixPrice($price)
    {
        $source = array('.', ',');
        $replace = array('', '.');
        $this->price = str_replace($source, $replace, $price);
    }

    protected function implodeSqlColumns()
    {
        return $this->setColumns = implode(", ", $this->setColumns);
    }

    protected function checkSize()
    {
        if (sizeof($this->setColumns) == 0) {
            header('location: index.php');
        }
        return true;
    }

    protected function isImage()
    {
        $extensions = ['png', 'jpeg', 'jpg', 'bmp', 'gif'];
        $ext = pathinfo($this->image, PATHINFO_EXTENSION);
        if (in_array($ext, $extensions)) {
            $this->removeSpaces();
            return true;
        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            return false;
        }
    }

    protected function removeSpaces()
    {
        $this->image = str_replace(' ', '', $this->image);
        $this->changeImage();
    }

    protected function changeImage()
    {
        $directory = 'products/' . $this->id . "/";

        $di = new RecursiveDirectoryIterator($directory, FilesystemIterator::SKIP_DOTS);
        $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($ri as $file) {
            $file->isDir() ? rmdir($file) : unlink($file);
        }

        copy(trim($_FILES['image']['tmp_name']), $directory . $this->image);

        return true;
    }

    public function update()
    {
        $this->populateSql();
        $this->sql .= $this->setColumns . " WHERE id=" . $this->id;

        Connection::connect()->query($this->sql);

        header('location:index.php');
    }
}

$update = new Update();
$update->update();
