<?php
require_once('config.php');

class Find
{
    protected function getId()
    {
        return $_GET['id'];
    }

    protected function giveSql($id)
    {
        return "SELECT * FROM products WHERE id=$id";
    }

    public function getProduct($id)
    {
        if ($result = mysqli_query(Connection::connect(), $this->giveSql($id))) {
            $row = mysqli_fetch_row($result);

            $product = new Product(
                $row[1],
                $row[2],
                $row[3],
                $row[4]
            );
            $product->setId($row[0]);
        }

        return $product;
    }

    public function getProductStore()
    {
        if ($result = mysqli_query(
            Connection::connect(),
            "SELECT * FROM products ORDER BY id DESC LIMIT 1"
        )) {
            $row = mysqli_fetch_row($result);

            $product = new Product(
                $row[1],
                $row[2],
                $row[3],
                $row[4]
            );
            $product->setId($row[0]);
        }

        return $product;
    }

    public function showProduct($location)
    {
        $product = $this->getProduct($this->getId());

        session_start();

        $_SESSION['product'] = $product;

        Connection::connect()->close();

        header("location:$location");
    }
}

$find = new Find();

if (isset($_POST['edit'])) {
    $find->showProduct('edit.php');
} else if (isset($_POST['show'])){
    $find->showProduct('show.php');
}
