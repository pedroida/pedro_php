<?php
require_once('config.php');

class ProductIndex
{
    public function getAll()
    {
        if ($query = $_GET['query']) {
            $result = Connection::connect()->query(
                "SELECT * FROM products WHERE title like '%$query%' OR description like '%$query%'"
            );
        } else {
            $result = Connection::connect()->query("SELECT * FROM products");
        }

        $products = [];
        while ($row = $result->fetch_assoc()) {
            $product = new Product(
                $row['title'],
                $row['description'],
                $row['price'],
                $row['image']
            );
            $product->setId($row['id']);
            array_push($products, $product);
        }

        return $products;
    }
}
