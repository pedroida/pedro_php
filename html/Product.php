<?php

class Product
{
    private $id;
    private $title;
    private $description;
    private $price;
    private $image;

    public function __construct($title, $description, $price, $image)
    {
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setPrice($price);
        $this->setImage($image);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = str_replace(" ", "", $image);
    }

    public function getPathImage()
    {
        return 'products/' . $this->getId() . "/" . $this->getImage();
    }
}
