<?php
require_once('config.php');
session_start();

$product = $_SESSION['product'];

require_once('header.php');
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="thumbnail">
                <img id="showImage" class="img-responsive" src="<?php echo $product->getPathImage(); ?>" alt="">
                <div class="caption well">
                    <h3><b>ID:</b> <?php echo $product->getId(); ?> </h3>
                    <h3><b>Título:</b> <?php echo $product->getTitle(); ?></h3>
                    <h3><b>Descrição:</b> <?php echo $product->getDescription(); ?> </h3>
                    <span class="badge"><?php echo 'R$ ' . number_format($product->getPrice(), 2, ',', '.') ?> </span>
                </div>
                <?php
                if ($_SESSION['logged']) {
                    ?>
                    <form action="Find.php?id=<?php echo $product->getId(); ?>" method="POST">
                        <button class="btn btn-warning" type="submit" name="edit">Editar produto</button>
                    </form>
                    <form action="Delete.php" method="POST">
                        <input type="hidden" name="id" value="<?php echo $product->getId(); ?>">
                        <button class="btn btn-danger" type="submit" name="button">Excluir</button>
                    </form>
                    <?php
                }
                ?>
                <a href="index.php" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
</body>
<?php include_once('footer.php'); ?>
</html>
