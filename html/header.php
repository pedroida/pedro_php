<html>

<head>
    <meta charset="utf-8"/>
    <title>Pedro</title>
    <link rel="stylesheet" href="css/app.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</head>

<body>
<div id="app">

<nav id="nav-sup" class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="index.php"><img id="indexImage" src="images/logo.png"
                                                              style="width:100px;"></a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                    <li class="navSearch">
                        <form class="form-inline" method="GET" action="index.php?=query">
                            <div class="form-group mx-sm-3 mb-2">
                                <input type="text" class="form-control" id="search" name="query"
                                       placeholder="O que procura..." required>
                            </div>
                            <button id="teste" type="submit" class="btn btn-info mb-2 ">Buscar</button>
                        </form>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <?php
                    if (!$_SESSION['logged']) {
                        ?>
                        <li class='navLink'><a href='login.php'>Entrar</a></li>
                        <li class='navLink'><a href='register.php'>Registrar</a></li>
                        <?php
                    } else {
                        ?>
                        <li><img class='img-circle' src=<?php echo $_SESSION['picture']; ?> style='width:4vw;'></li>
                        <li class='dropdown navLink'>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button'
                               aria-expanded='false' aria-haspopup='true'>
                                <?php echo $_SESSION['name']; ?><span class='caret'></span></a>
                            <div class="dropdown-menu navLink" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="LoginIndex.php">Sair</a>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
