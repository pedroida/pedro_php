<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 01/03/18
 * Time: 10:41
 */

class ValidateStore
{
    private $inputs;
    private $image;

    public function __construct($inputs = "", $image = "")
    {
        $this->inputs = $inputs;
        $this->image = $image;
    }

    public function validateInputs()
    {
        foreach ($this->inputs as $input) {
            if (empty(trim($input))) {
                return false;
            }
        }

        if(empty(trim($this->image))) {
            return false;
        }
        return true;
    }
}