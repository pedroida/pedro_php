<div id="cont-footer" class="container">
    <div id="footer">
        <p class="text-muted credit"><span style="text-align: center; float: left">&copy; 2017 <a href="#">Pedro Ida</a></span>
            <span class="hidden-phone"
                  style="text-align: right; float: right">Powered by: <a
                        href="http://laravel.com/" alt="Laravel 5.1">Laravel 5.5</a></span></p>
    </div>
</div>
