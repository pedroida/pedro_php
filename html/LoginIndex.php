<?php
session_start();
require_once('config.php');


class LoginIndex
{
    private $user;

    public function __construct()
    {
        $this->user = new User($_POST['email'], $_POST['password']);
    }

    public function login()
    {
        $email = $this->user->getEmail();
        $password = $this->user->getPassword();

        $result = Connection::connect()->query("SELECT * FROM users WHERE email='$email' AND password='$password'");
        if (mysqli_num_rows($result) > 0) {

            $row = $result->fetch_assoc();

            $name = $row['name'];
            $picture = $row['pic'];

            unset($_SESSION['error_login']);

            $_SESSION['name'] = $name;
            $_SESSION['picture'] = $picture;
            $_SESSION['logged'] = true;

            header('location:index.php');
        } else {
            $_SESSION['logged'] = false;
            $_SESSION['error_login'] = true;
            header('location:login.php');
        }
    }

    public function logout()
    {
        unset($_SESSION['name']);
        unset($_SESSION['logged']);
        header('location:login.php');

    }
}

$login = new LoginIndex();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $login->login();
} else {
    if ($_SESSION['logged']) {
        $login->logout();
    }
}
