<?php
session_start();
require_once('config.php');

require_once('header.php');

$productIndex = new ProductIndex();
$products = $productIndex->getAll();
?>
<div class="container">
    <div class="row text-center panel">
        <div class="page-header">
            <h1>Lista de Produtos</h1>
        </div>
        <?php
        if ($_SESSION['logged']) {
            ?>
            <div class="alert ">
                <a class="btn btn-success col-md-2" href="create.php" role="button">Adicionar produto</a>
            </div>
            <?php
        }
        ?>
        <div class="panel-body col-md-12">
            <?php
            foreach ($products as $product) {
                ?>
                <div class="col-md-2 col-lg-3  card">
                    <img style="max-width: 100%;" id="productImage" src="<?php echo $product->getPathImage(); ?>"
                         alt="<?php echo $product->getImage(); ?>">
                    <div class="in-card caption well">
                        <h4><b>Título:</b><br>
                            <div id="titulo">
                                <?php echo $product->getTitle(); ?>
                            </div>
                        </h4>
                        <span class="badge"><?php echo 'R$ ' . number_format($product->getPrice(), 2, ',', '.'); ?> </span>
                    </div>
                    <div class="btn-card">
                        <form action="Find.php?id=<?php echo $product->getId(); ?>" method="POST">
                            <button class="btn btn-primary" type="submit" name="show">Visualizar</button>
                        </form>
                        <?php
                        if ($_SESSION['logged']) {
                            ?>
                            <form action="Find.php?id=<?php echo $product->getId(); ?>" method="POST">
                                <button class="btn btn-warning" type="submit" name="edit">Editar produto</button>
                            </form>
                            <form action="Delete.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $product->getId(); ?>">
                                <button class="btn btn-danger" type="submit" name="button">Excluir</button>
                            </form>
                            <?php
                        } ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</div>
</body>
<?php include_once('footer.php'); ?>
</html>
