<?php
require_once('config.php');

session_start();

VerifyLogin::isLogged();

require_once('header.php');

?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cadastrar Produto
                </div>
                <div class="panel-body well">
                    <form id="myForm" action="Store.php" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Descrição</label>
                            <input type="text" name="description" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="price">Preço</label>
                            <input type="text" name="price" class="form-control price" required>
                        </div>
                        <div class="form-group">
                            <label for="image">Imagem</label>
                            <input type="file" name="image" class="form-control" required>
                        </div>

                        <button class="btn btn-success" type="submit" value="submit" name="button">Salvar</button>
                        <a class="btn btn-info" href="index.php">Voltar</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include_once('footer.php'); ?>
</html>
<script type="text/javascript" src="js/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="js/masks.js"></script>
